module.exports = {
  chainWebpack: config => {
    config
    .plugin('html')
    .tap(args => {
      args[0].title = 'Parth Patel | Software Engineer'
      return args
    })
  },
  pwa: {
    workboxPluginMode: 'InjectManifest',
    workboxOptions: {
      swSrc: 'service-worker.js'
    }
  },
  css: {
    loaderOptions: {
      sass: {
        prependData: `@import '@/assets/css/base.scss';`
      }
    }
  }
}
