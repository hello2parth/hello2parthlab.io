export const EXPERIENCES = [
  {
    title: {
      text: 'SQL Mock App',
      link: 'https://hello2parth.gitlab.io/sql-mock-app/',
    },
    demo: {
      web: 'https://hello2parth.gitlab.io/sql-mock-app/',
      code: 'https://gitlab.com/hello2parth/sql-mock-app',
    },
    points: [
      'A resizable SQL Editor for writing the Query',
      `Editor's Action Bar: Run and Kill the Query`,
      `Table showing the Query's results`,
      'Status Bar displays Query execution timings, status, and row count',
    ],
    tags: ['react', 'react-hooks', 'react-table', 'react-window', 'web-worker'],
  },
  {
    title: {
      text: 'Mutual Fund Dashboard',
      link: 'https://hello2parth.gitlab.io/mutual-fund-dashboard/',
    },
    demo: {
      web: 'https://hello2parth.gitlab.io/mutual-fund-dashboard/',
      code: 'https://gitlab.com/hello2parth/mutual-fund-dashboard',
    },
    points: [
      'Displays a list of Mutual funds available in India',
      'Sort, filter, and search Mutual funds',
      'Selecting a fund displays its details',
      'Option to choose theme and language',
    ],
    tags: ['vue-cli', 'vuex', 'vue-router', 'i18n'],
  },
  {
    title: {
      text: 'YouTube Transcript Editor',
      link: 'https://hello2parth.gitlab.io/youtube-video-transcript-editor/',
    },
    demo: {
      web: 'https://hello2parth.gitlab.io/youtube-video-transcript-editor/',
      code: 'https://gitlab.com/hello2parth/youtube-video-transcript-editor',
    },
    points: [
      'Highlights the caption in transcript with video content',
      'Transcript can be corrected/editable',
      'Video automatically pause when user starts editing',
    ],
    tags: ['vue.js', 'youtube-iframe'],
  },
  {
    title: {
      text: 'What I did @SonicWall',
      link: 'https://www.sonicwall.com/',
    },
    points: [
      `Developed a UI for SonicWall's firewall management system like sonicOS and GMS`,
      'Implemented a CRUD operations using various type of APIs like REST, tunnel, JSON etc.',
      'Plotted real time Packet monitor graph and also implemented lazy loading in table',
    ],
    tags: ['vue.js', 'typescript', 'scss', 'node.js'],
  },
  {
    title: {
      text: 'Chart Library',
      link: 'https://hello2parth.gitlab.io/chart-library/',
    },
    demo: {
      web: 'https://hello2parth.gitlab.io/chart-library/',
      code: 'https://gitlab.com/hello2parth/chart-library',
    },
    points: [
      'Responsive, Customizable, Light-weight',
      `Single bundled file for all three chart`,
      'Line chart, Line-Bar chart, Gauge chart',
    ],
    tags: ['d3.js', 'webpack'],
  },
  {
    title: {
      text: 'What I did @Jio',
      link: 'https://www.jio.com/',
    },
    points: [
      `Developed web applications to analyze and research Jio’s customer data for Jio Music, Jio TV etc.`,
      'Worked on real-time customer onboarding application with the help of Mapbox and custom controls',
      'Developed various applications like Sales Dashboard, Inbound-Outbound Data Management etc.',
    ],
    tags: ['vue.js', 'sapui5', 'jQuery', 'mocha-chai'],
  },
  {
    title: {
      text: 'What I did @LTI',
      link: 'https://www.lntinfotech.com/',
    },
    points: [
      'I worked for various clients like HD Supply, Infinion and some internal projetcs as well',
      'Developed Merchant-vendor negotiations, order and inventory management',
      'Developed HR portal for visa, leave application and to view awards and attendance etc ',
    ],
    tags: ['sapui5', 'jQuery', 'require.js', 'html5', 'css3'],
  },
];
