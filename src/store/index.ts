import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    isThemeLight: false,
  },
  mutations: {
    updateTheme(state, isThemeLight) {
      state.isThemeLight = isThemeLight;
    }
  },
  actions: {
  },
  modules: {
  },
});
