import Vue from 'vue';
import VueRouter, { RouteConfig } from 'vue-router';
import Home from '../views/Home.vue';

Vue.use(VueRouter);

const routes: RouteConfig[] = [
  {
    path: '/',
    name: 'Home',
    component: Home,
    meta: {
      title: 'Parth Patel | Software Engineer',
      metaTags: [
        {
          name: 'theme-color',
          content: '#262626'
        },
        {
          name: 'msapplication-navbutton-color',
          content: '#262626'
        },
        {
          name: 'apple-mobile-web-app-status-bar-style',
          content: '#262626'
        }
      ]
    }
  },
];

const router = new VueRouter({
  routes,
});

export default router;
